//Soal No. 1 (Array to Object)

function arrayToObject(arr) {
    // Code di sini
    var now = new Date()
    var thisYear = now.getFullYear()
    var result = ""

    if(arr.length == 0 || arr[0].length == 0){
        console.log('""')
    }else{
        for(var i = 0; i < arr.length; i++){
            var obj = {
                ["firstName"]: [arr[i][0]],
                ["lastName"]: [arr[i][1]],
                ["gender"]: [arr[i][2]],
                ["age"]: [thisYear - arr[i][3]]
            }
            if(obj.age == "NaN" || obj.age < 0){
                obj.age = "Invalid Birth Year"
            }
            result += i+1+". "+obj.firstName+" "+obj.lastName+": {\n\t"+
                "firstName: \""+obj.firstName+"\",\n\t"+
                "lastName: \""+obj.lastName+"\",\n\t"+
                "gender: \""+obj.gender+"\",\n\t"+
                "age: \""+obj.age+"\"\n}\n"
            
        }
        console.log(result)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("\n")
//Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    var price = {
        [0] : 1500000,
        [1] : 500000,
        [2] : 250000,
        [3] : 175000,
        [4] : 50000
    }
    var brand = {
        [0] : 'Sepatu Stacattu',
        [1] : 'Baju Zoro',
        [2] : 'Baju H&N',
        [3] : 'Sweater Uniklooh',
        [4] : 'Casing Handphone'
    }

    var listPurchased = []
    var changeMoney = money

    var result = {}
    // you can only write your code here!
    if(memberId == "" || memberId == null){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }else{
        for(var i = 0; i<5; i++){
            if(changeMoney - price[i] >= 0){
                listPurchased.push(brand[i])
                changeMoney -= price[i]
            }
        }
        result['memberId'] = memberId
        result['money'] = money
        result['listPurchased'] = listPurchased
        result['changeMoney'] = changeMoney
        
        return result
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  console.log("\n\n")


  //Soal No. 3 (Naik Angkot)
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var result = []
    for(var i=0; i<arrPenumpang.length; i++){
        var obj = {
            ['penumpang'] : arrPenumpang[i][0],
            ['naikDari'] : arrPenumpang[i][1],
            ['tujuan'] : arrPenumpang[i][2]
        }
        obj.bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))*2000
        result.push(obj)
    }
    return result
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]