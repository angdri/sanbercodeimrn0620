import React from 'react'
import {StyleSheet, Text, View, Touchable, Image, TextInput, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons'

export const LoginScreen = ({navigation}) => {
    return(
        <View style={styles.container}>
            <Image source={require('./images/logo.png')} style={styles.header}/>
            <View style={styles.formLayout}>
                <Text style={styles.headerText}>Login</Text>
                <View>
                    <Text style={styles.textTitle}>Username/Email</Text>
                    <TextInput style={styles.textInputRegister}/>
                    <Text style={styles.textTitle}>Password</Text>
                    <TextInput style={styles.textInputRegister}/>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        style={styles.btnStyleDark}
                        onPress={() => navigation.push("Home")}
                    >
                        <Text style={{color:'white', fontSize: 20}}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={{alignSelf: 'center', marginTop: 10}}>atau</Text>
                    <TouchableOpacity
                        style={styles.btnStyleLight}
                        onPress={() => navigation.push("Register")}
                    >
                        <Text style={{color: 'white', fontSize: 20}}>Daftar ?</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    formLayout:{
        alignSelf: 'center',
        justifyContent: 'center',
        flex: 0.8
    },
    headerText: {
        alignSelf: 'center',
        justifyContent: 'center',
        fontSize: 25,
        color: '#003366',
    },
    
    textTitle:{
        paddingTop: 10,
    },
    textInputRegister: {
        height: 40,
        width: 300,
        // alignSelf: 'center',
        borderColor: 'grey',
        borderWidth: 1,
    },
    btnContainer: {
        marginVertical: 15,
    },
    btnStyleDark:{
        backgroundColor: '#003366',
        width: 120,
        height: 40,
        borderRadius: 15,
        fontSize: 20,
        textAlign: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'

    },
    btnStyleLight:{
        marginTop: 10,
        backgroundColor: '#3EC6FF',
        width: 120,
        height: 40,
        borderRadius: 15,
        fontSize: 20,
        alignSelf: 'center',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'

    }

})