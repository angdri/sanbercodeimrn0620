import React from "react";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { AboutScreen } from './AboutScreen'
import { AddScreen } from './AddScreen'
import { LoginScreen } from './LoginScreen'
import { ProjectScreen } from './ProjectScreen'
import { SkillScreen } from './SkillScreen'
import { RegisterScreen } from './RegisterScreen'

const LoginStack = createStackNavigator()

const AboutStack = createStackNavigator()
const AboutStackScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen name="About" component={AboutScreen}/>
    </AboutStack.Navigator>
)

const Tabs = createBottomTabNavigator()
const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillScreen} />
        <Tabs.Screen name="Project" component={ProjectScreen} />
        <Tabs.Screen name="Add" component={AddScreen} />
    </Tabs.Navigator>
)

const Drawer = createDrawerNavigator()
const HomeScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabsScreen}/>
        <Drawer.Screen name="About" component={AboutStackScreen}/>
    </Drawer.Navigator>
)

export default () => (
    
    <NavigationContainer>
        <LoginStack.Navigator>
            <LoginStack.Screen name="Login" component={LoginScreen}/>
            <LoginStack.Screen name="Register" component={RegisterScreen}/>
            <LoginStack.Screen name="Home" component={HomeScreen}/>
        </LoginStack.Navigator>
    </NavigationContainer>
)