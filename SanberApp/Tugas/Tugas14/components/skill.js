import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AntDesign } from '@expo/vector-icons';

export default class Skill extends Component {
    
    render(){
        let skill = this.props.skill
        
        return(
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image source={{uri : skill.logoUrl}} style={{ width:70, height:70}} />
                </View>
                <View style={styles.skillDetailContainer}>
                    <Text style={{color: '#003366', fontSize: 25, fontWeight:'bold'}}>{skill.skillName}</Text>
                    <Text style={{color: '#3EC6FF', fontSize: 15}}>{skill.categoryName}</Text>
                    <Text style={{color: 'white', fontSize: 30, width: '45%', alignSelf: 'flex-end', fontWeight:'bold', textAlign: 'right'}}>{skill.percentageProgress}</Text>
                </View>
                <View style={styles.rightIconContainer}>
                    <AntDesign name="right" size={50} color="#003366" />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#B4E9FF',
        flexDirection: 'row',
        borderRadius: 5,
        marginHorizontal: 10,
    },
    logoContainer:{
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    skillDetailContainer: {
        flex: 0.5, 
    },
    rightIconContainer: {
        flex: 0.25,
        alignItems: 'center',
        justifyContent: 'center'
    }
})