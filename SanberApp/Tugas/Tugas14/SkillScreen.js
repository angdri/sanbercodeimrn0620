import React from 'react';
import { View, Text, Image, ScrollView, TextInput, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { FontAwesome, FontAwesome5, AntDesign } from '@expo/vector-icons'; 
import data from './skillData.json'
import Skill from './components/skill';

export default class Main extends React.Component {
    render(){
        return (
            <View styles={styles.container}>
                <Image style={styles.imageHeader} source={require('./images/logo.png')}/>
                <View style={styles.welcomeContainer}>
                    <FontAwesome style={styles.profilePict} name="user-circle" size={25} color="#3EC6FF" />
                    <View style={styles.welcomeText}>
                        <Text style={{fontSize: 15, color: '#003366'}}>Hai,</Text>
                        <Text style={{fontSize: 18, color: '#003366'}}>Mukhlis Hanafi</Text>
                    </View>
                </View>
                <Text style={{ marginHorizontal: 10, fontSize: 36, color: '#003366', fontWeight:'300'}}>SKILL</Text>
                <View style={{marginHorizontal: 10, borderBottomWidth: 3, borderColor: '#3EC6FF'}}></View>
                <View style={styles.skillTabContainer}>
                    <View style={styles.skillTab}>
                        <Text style={styles.skillText}>Library/Framework</Text>
                    </View>
                    <View style={styles.skillTab}>
                        <Text style={styles.skillText}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.skillTab}>
                        <Text style={styles.skillText}>Teknologi</Text>
                    </View>
                </View>
                <View style={styles.skillContainer}>
                    <FlatList
                        data = {data.items}
                        renderItem={(skill) => <Skill skill={skill.item}/>}
                        keyExtractor={(item) => item.id}
                        ItemSeparatorComponent={() => <View style={{marginBottom:5}}/>}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageHeader: {
        width: 200,
        height: 60,
        alignSelf: 'flex-end',
    },
    welcomeContainer: {
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    profilePict: {
        marginHorizontal: 10,
        justifyContent: 'center',
    },
    welcomeText: {
        flexDirection: 'column',
    },
    skillTabContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    skillTab: {
        height: 30,
        marginVertical: 5,
        paddingHorizontal: 6,
        backgroundColor: '#B4E9FF',
        borderRadius: 10,
        justifyContent: 'center'
    },
    skillText: {
        fontSize: 15,
        color: '#003366',
    }
})
