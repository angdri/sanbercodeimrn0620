import React from 'react'
import {StyleSheet, Text, View, Touchable, Image, TextInput, TouchableOpacity} from 'react-native'
import { FontAwesome, FontAwesome5, AntDesign } from '@expo/vector-icons'; 

export default class App extends React.Component {
    render() {
        return(
            <View style={styles.container}>
                <Text style={styles.headerText}>Tentang Saya</Text>
                <FontAwesome style={styles.profilePict} name="user-circle" size={180} color="#e5e5e5" />
                <View style={{marginTop: 35, marginBottom: 15}}>
                    <Text style={styles.textName}>Mukhlish Hanafi</Text>
                    <Text style={styles.textJob}>React Native Developer</Text>
                </View>
                <View style={styles.containerGrey}>
                    <Text style={{fontSize: 18, marginHorizontal: 15}}>Portofolio</Text>
                    <View style={{borderBottomWidth: 1, marginHorizontal: 15}}></View>
                    <View style={styles.gitContainer}>
                        <View style={styles.gitStyle}>
                            <FontAwesome style={{alignSelf: 'center'}} name="gitlab" size={50} color="#3EC6FF" />
                            <Text style={styles.gitFont}>@mukhlish</Text>
                        </View>
                        <View style={styles.gitStyle}>
                            <FontAwesome style={{alignSelf: 'center'}} name="github" size={50} color="#3EC6FF" />
                            <Text style={styles.gitFont}>@mukhlis-h</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.containerGrey}>
                    <Text style={{fontSize: 18, marginHorizontal: 15}}>Hubungi Saya</Text>
                    <View style={{borderBottomWidth: 1, marginHorizontal: 15}}></View>
                    <View style={styles.hubungiContainer}>
                        <View style={styles.hubungiStyle}>
                            <View style={{alignItems: 'center', flex: 0.4}}>
                                <FontAwesome5 style={{alignSelf: 'center', alignItems: 'center'}} name="facebook-f" size={50} color="#3EC6FF" />
                            </View>
                            
                            <Text style={styles.hubungiFont}>mukhlish.hanafi</Text>
                        </View>
                        <View style={styles.hubungiStyle}>
                            <View style={{alignItems: 'center', flex: 0.4}}>
                                <AntDesign style={{alignSelf: 'center', alignItems: 'center'}} name="instagram" size={50} color="#3EC6FF" />
                            </View>
                           
                            <Text style={styles.hubungiFont}>@mukhlish_hanafi</Text>
                        </View>
                        <View style={styles.hubungiStyle}>
                            <View style={{alignItems: 'center', flex: 0.4}}>
                                <AntDesign style={{alignSelf: 'center', alignItems: 'center'}} name="twitter" size={50} color="#3EC6FF" />
                            </View>
                            
                            <Text style={styles.hubungiFont}>@mukhlish</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    profilePict: {
        marginVertical: 35,
        alignSelf: 'center'
    },
    headerText: {
        marginVertical: 25,
        width: '57%',
        alignSelf: 'center',
        fontSize: 36,
        color: '#003366',
        fontWeight: 'bold',
        justifyContent: 'center'
    },
    profilePict: {
        alignSelf: 'center'
    }, 
    textName:{
        fontSize: 25,
        alignSelf: 'center',
        color: '#003366',
        fontWeight: 'bold',
        width: '50%'
    },
    textJob:{
        alignSelf: 'center',
        color: '#3EC6FF',
    },
    containerGrey: {
        marginVertical: 5,
        width: '90%',
        backgroundColor: '#EFEFEF',
        borderRadius: 25,
        alignSelf: "center"
    },
    gitContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    gitStyle: {
        marginVertical: 15,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    gitFont:{
        width: '108%',
        color: '#003366',
        fontSize:15,
        fontWeight: 'bold'
    },
    hubungiContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    hubungiStyle: {
        width: '65%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10
    },
    hubungiFont: {
        flex: 0.6,
        color: '#003366',
        fontSize:15,
        fontWeight: 'bold'
    }

    
})