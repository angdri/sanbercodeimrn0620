import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import YoutubeUI from './Tugas/Tugas12/App'
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import RegisterScreen from './Tugas/Tugas13/RegisterScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import ToDoApp from './Tugas/Tugas14/App'
import Tugas15 from './Tugas/Tugas15/index'
import TugasNavigation from './Tugas/TugasNavigation/index'
import Quiz3 from './Quiz3/index'

export default function App() {
  return (
    // <LoginScreen />
    // <RegisterScreen />
    // <AboutScreen />
    // <Tugas15 />
    // <TugasNavigation />
    <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
