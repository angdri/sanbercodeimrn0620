//Soal No. 1 (Callback Baca Buku)
// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
timer = 10000
i=0
readBooks(timer, books[i],function funtime(time){
    if(i < books.length-1){
        i++
        readBooks(time, books[i],funtime)
    }
})


