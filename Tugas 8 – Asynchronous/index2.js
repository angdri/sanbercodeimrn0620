var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
timer = 10000
i=0
function funerr (time){
    console.log(time)
}

readBooksPromise(timer,books[i])
    .then(function funfull (time){
        if(i < books.length-1){
            i++
            readBooksPromise(time, books[i])
            .then(funfull)
            .catch(funerr)
        }
    })
    .catch(funerr)
