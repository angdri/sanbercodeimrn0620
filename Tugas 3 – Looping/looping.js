//No. 1 Looping While 
console.log("LOOPING PERTAMA")
var i = 0
while(i < 20){
    i+=2
    console.log(i + " - I love coding")
}

console.log("LOOPING KEDUA")
while(i >= 2){
    console.log(i + "  - I will become a mobile developer")
    i -= 2
}

//No. 2 Looping menggunakan for
console.log("\n\nOUTPUT")
for(var i = 1; i<=20; i++){
    if(i%2 != 0 && i%3 == 0){
        console.log(i + " - I Love Coding")
    }else if(i%2 != 0){
        console.log(i + " - Santai")
    }else if(i%2 == 0){
        console.log(i + " - Berkualitas")
    }
}

//No. 3 Membuat Persegi Panjang
console.log("\n")
var p = 8
var l = 4
var pp = ""
for(var i = 0; i<l; i++){
    for(var j=0; j<p; j++){
        pp+='#'
    }
    pp+="\n"
}
console.log(pp)

//No. 4 Membuat Tangga 
var t = 7
var st = ""
for(var i = 0; i<=t; i++){
    for(var j=0; j<i; j++){
        st+='#'
    }
    st+="\n"
}
console.log(st)

//No. 5 Membuat Papan Catur
var p = 8
var l = 8
var pc = ""
for(var i = 0; i<l; i++){
    for(var j=0; j<p/2; j++){
        if(i%2!=0){
            pc+=' #'
        }else{
            pc+='# '
        }
    }
    pc+="\n"
}
console.log(pc)