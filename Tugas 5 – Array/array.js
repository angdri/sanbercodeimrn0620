//Soal No. 1 (Range)

function range(startNum = -1, finishNum = -1){
    if(startNum == -1 || finishNum == -1){
        return -1
    }else{
        var result = []
        if(startNum < finishNum){
            for(var i = startNum; i <= finishNum; i++){
                result.push(i)
            }
        }else{
            for(var i = startNum; i >= finishNum; i--){
                result.push(i)
            }
        }
        return result
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log("\n\n")
//Soal No. 2 (Range with Step)
function rangeWithStep(startNum=-1, finishNum=-1, step) {
    if(startNum == -1 || finishNum == -1){
        return -1
    }else{
        var result = []
        if(startNum < finishNum){
            for(var i = startNum; i <= finishNum; i++){
                result.push(i)
                i += (step-1)
            }
        }else{
            for(var i = startNum; i >= finishNum; i--){
                result.push(i)
                i -= (step-1)
            }
        }
        return result
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log("\n\n")

//Soal No. 3 (Sum of Range)
function sum(startNum = -1, finishNum = -1, step =1) {
    if(startNum == -1 && finishNum == -1){
        return 0
    }else if(startNum != -1 && finishNum == -1){
        return startNum
    }else{
        var result = 0
        if(startNum < finishNum){
            for(var i = startNum; i <= finishNum; i++){
                result+=i
                i += (step-1)
            }
        }else{
            for(var i = startNum; i >= finishNum; i--){
                result+=i
                i -= (step-1)
            }
        }
        return result
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log("\n\n")

//Soal No. 4 (Array Multidimensi)
function dataHandling(inputArray){
    var result = ""
    var sizeInput = inputArray.length
    for(var i=0; i<sizeInput; i++){
        result += "Nomor ID: "+inputArray[i][0]+
        "\nNama Lengkap: "+inputArray[i][1]+
        "\nTTL:  "+inputArray[i][2]+" "+inputArray[i][3]+
        "\nHobi:  "+inputArray[i][4]+"\n\n"
    }
    return result
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input))

//Soal No. 5 (Balik Kata)
function balikKata(kata){
    var result = ""
    for(var i=(kata.length -1); i>=0; i--){
        result += kata[i]
    }
    return result
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log("\n\n")
//Soal No. 6 (Metode Array)
function dataHandling2(inputArray){
    var nama = inputArray.slice(1,2)+"Elsharawy"
    var provinsi = "Provinsi "+inputArray.slice(2,3)
    var jenisKelamin = "Pria"
    var sekolah = "SMA Internasional Metro"
    var tanggal = inputArray.slice(3,4)[0].split("/")
    var bulan = ""

    switch(tanggal[1]) {
        case '01':   { bulan = 'Januari'; break; }
        case '02':   { bulan = ' Februari '; break; }
        case '03':   { bulan = ' Maret '; break; }
        case '04':   { bulan = ' April '; break; }
        case '05':   { bulan = ' Mei '; break; }
        case '06':   { bulan = ' Juni '; break; }
        case '07':   { bulan = ' July '; break; }
        case '08':   { bulan = ' Agustus '; break; }
        case '09':   { bulan = ' September '; break; }
        case '10':   { bulan = ' Oktober '; break; }
        case '11':   { bulan = ' November '; break; }
        case '12':   { bulan = ' Desember '; break; }
        default:  { bulan = 'Bulan salah'; }}
    

    inputArray.splice(1,2,nama,provinsi)
    inputArray.splice(4,1,jenisKelamin, sekolah)
    console.log(inputArray)
    console.log(bulan)
    console.log(tanggal.slice(0).sort(function(value1, value2){ return value2 - value1}))
    console.log(tanggal.join('-'))
    console.log(inputArray.slice(1,2)[0].slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", 
"Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung",
 * "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 